# ripdeque

A simple queue server inspired by [`beanstalkd`](https://beanstalkd.github.io/).

For now, the server is interacted with using a simple plaintext TCP connection:

```sh
> PUT <item><EOF>
< <queue length><EOF>

> GET
< <item OR blank if empty><EOF>
```

`ripdeque` will always return as soon as possible, even if the queue is empty. If nothing is in the cache, clients know as soon as possible so they can load from another resource. This is NOT a work queue where you want to block for the next available work.
